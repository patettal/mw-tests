
var frisby = require('frisby');
// Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});

//there should be better ways to do this!
 var URL = 'https://middleware-player.siplay.com'; //prod
//var URL = 'https://middleware-player.siplay.xyz'; //xyz
var responsetime=10000; 

/*
POST TO https://middleware-player.siplay.xyz/event
Auth: 0774c443b5c84c25041aa7b753835df8d4857ca0a1604ad36fbd17702e35c082887869208ce8db6e438506ae421973715692baaa866888cba975759e552c2a91
Request params:
{
    "eventName": "",
    "homeTeamGuid": "SS-563298",
    "eventGuid": "",
    "homeTeamName": "Tigers",
    "startTS": 1476210922000,
    "description": "yooo",
    "sendNotification": true,
    "recipients": ["COACHES", "OFFICIALS", "MEMBERS"],
    "durationMinutes": 60,
    "awayTeamName": "testytttttt",
    "resourceId": "SS-48845",
    "type": "Game",
    "awayTeamGuid": "",
    "teamGuid": "SS-563298"
}
*/

//existing user with XREF in LA and SS
var email = 'luciaines.patetta%40gmail.com';
var password = 'SIPtest2';

var params = {};
params = {
    "eventName": "Name", //unused by app, verifications break BACKLOGGED
    "homeTeamGuid": "SS-563298",
    "homeTeamId":563298,
    "eventGuid": "",
    "homeTeamName": "Tigers",
    "startTS": 1477821600000,
    "startTS_String":'2016-10-30T10:00:00Z',
    "durationMinutes": 60,
    "endTS_String":'2016-10-30T11:00:00Z',
    "description": "testttt",
    "sendNotification": true,
    "recipients": ["COACHES", "OFFICIALS", "MEMBERS"],
    "awayTeamName": "yooooo",
    "resourceId": "SS-48845",
    "resourceIdSSU":48845,
    "type": "Game",
    "awayTeamGuid": "",
    "teamGuid": "SS-563298"
};


frisby.create("Log in to user "+email)
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)

    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        params['type']="Game";
        frisby.create("Base game creation")
            .post(URL + "/event",{
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                AwayTeam:{
                    Name:params['awayTeamName']
                },
                Resource:{
                    Id:params['resourceIdSSU']
                }
            })
            .toss();

            //Variating tests
                            
        params['type']="Other";
        frisby.create("Base type:Other creation")
            .post(URL + "/event",{
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                // AwayTeam:{
                //     Name:params['awayTeamName']
                // },
                Resource:{
                    Id:params['resourceIdSSU']
                }
            })
             .toss();

        params['type']="Practice";
        frisby.create("Base type:Practice creation")
            .post(URL + "/event",{
                 //eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                // AwayTeam:{
                //     Name:params['awayTeamName']
                // },
                Resource:{
                    Id:params['resourceIdSSU']
                }
            })
            .toss();


        params['type']="Game";
        params["resourceId"]= "SS-61526";
        params["resourceIdSSU"]=61526;
        frisby.create("Game in different Resource")
            .post(URL + "/event",{
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                AwayTeam:{
                    Name:params['awayTeamName']
                },
                Resource:{
                    Id:params['resourceIdSSU']
                }
            })
            .toss();

        params['type']="Game";
        params["resourceId"]= "";
        params["resourceIdSSU"]=0;
        frisby.create("Game in TBD Resource (left blank)")
            .post(URL + "/event",{
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                AwayTeam:{
                    Name:params['awayTeamName']
                },
                Resource:{
                    Id:params['resourceIdSSU']
                }
            })
            .toss();

        params['type']="Game";
        params["resourceId"]= "";
        params["resourceIdSSU"]=0;
        params["customResourceName"]="Avenida Córdoba & Calle Medrano, Buenos Aires Autonomous City of Buenos Aires Argentina";
        frisby.create("Game in Custom Resource")
            .post(URL + "/event",{
                 customResourceName:params["customResourceName"], //special key for this scenario
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                AwayTeam:{
                    Name:params['awayTeamName']
                },
                Resource:{
                    Id:params['resourceIdSSU'],
                    Name:params['customResourceName']
                }
            })
            .toss();

        //restoring params
        params["resourceId"]= "SS-48845";
        params["resourceIdSSU"]=48845;


        params['type']="Game";
        params["sendNotification"]= false;
        frisby.create("Game with no notification")
            .post(URL + "/event",{
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 //recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                AwayTeam:{
                    Name:params['awayTeamName']
                },
                Resource:{
                    Id:params['resourceIdSSU']
                }
            })
            .toss();

                            // params = {
                            //     "eventName": "", //verifications break BACKLOGGED
                            //     "homeTeamGuid": "SS-563298", //TC: Different Team for a different user, LA, maybe non-coach if time permits
                            //     "homeTeamId":563298,
                            //     "eventGuid": "",
                            //     "homeTeamName": "Tigers",
                            //     "startTS": 1477821600000, //TC Time and duration changes
                            //     "startTS_String":'2016-10-30T10:00:00Z',
                            //     "durationMinutes": 60, //TC: Different Duration 
                            //     "endTS_String":'2016-10-30T11:00:00Z',
                            //     "description": "testttt",
                            //     "sendNotification": true, //TC: not notify
                            //     "recipients": ["COACHES", "OFFICIALS", "MEMBERS"],
                            //     "awayTeamName": "yooooo",
                            //     "resourceId": "SS-48845",// TC: Different ResourceID, TBD Resource, User Input
                            //     "resourceIdSSU":48845, //TC: Different resourceId
                            //     "type": "Game", //TC: Practice, Other
                            //     "awayTeamGuid": "",
                            //     "teamGuid": "SS-563298"
                            // };

        params['type']="Game";
        params["startTS"]= 1478271600000;
        params["startTS_String"]='2016-11-04T15:00:00Z';
        params["durationMinutes"]= 30;
        params["endTS_String"]='2016-11-04T15:30:00Z';
        frisby.create("Game with different time settings")
            .post(URL + "/event",{
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 //recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .expectJSON('*',{
                //Name:params['eventName'],
                Description : params['description'],
                Start:params['startTS_String'],
                End:params['endTS_String'],
                Type:params['type'],
                HomeTeam:{
                    Id:params['homeTeamId']
                },
                AwayTeam:{
                    Name:params['awayTeamName']
                },
                Resource:{
                    Id:params['resourceIdSSU']
                }
            })
            .toss();

/* ------ NEEDS FURTHER WORK------
        //Editing test cases
        //back to basic setup just in case
        params = {
            "eventName": "Name", //unused by app, verifications break BACKLOGGED
            "homeTeamGuid": "SS-563298",
            "homeTeamId":563298,
            "eventGuid": "",
            "homeTeamName": "Tigers",
            "startTS": 1477821600000,
            "startTS_String":'2016-10-30T10:00:00Z',
            "durationMinutes": 60,
            "endTS_String":'2016-10-30T11:00:00Z',
            "description": "testttt",
            "sendNotification": true,
            "recipients": ["COACHES", "OFFICIALS", "MEMBERS"],
            "awayTeamName": "yooooo",
            "resourceId": "SS-48845",
            "resourceIdSSU":48845,
            "type": "Game",
            "awayTeamGuid": "",
            "teamGuid": "SS-563298"
        };

        frisby.create("Base game creation")
            .post(URL + "/event",{
                 eventName: params['eventName'],
                 homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                 eventGuid: params['eventGuid'], //blank param, no user control
                 homeTeamName: params['homeTeamName'], //don't care to test
                 startTS: params['startTS'], //checked
                 description: params['description'],//checked
                 sendNotification: params['sendNotification'],//doesn't appear in response
                 recipients: params['recipients'],//doesn't appear in response
                 durationMinutes: params['durationMinutes'],//check via End key
                 awayTeamName: params['awayTeamName'], //checked
                 resourceId: params['resourceId'], //checked via resourceIdSSU
                 type: params['type'], //checked
                 awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                 teamGuid: params['teamGuid']//checked with homeTeamId

            },{json: true})
            //.inspectJSON()
            .after(function (err, res, body) {
                //var obj = JSON.parse(body);  
                console.log(body);
                //var eventGuid = obj[0]['Id'];
                //console.log(eventGuid);

                params["resourceId"]= "SS-61526";
                params["resourceIdSSU"]=61526;
                // frisby.create("Edit Game to different Resource")

                //     .post(URL + "/event",{
                //          eventName: params['eventName'],
                //          homeTeamGuid: params['homeTeamGuid'], //checked with homeTeamId
                //          eventGuid: params['eventGuid'], //blank param, no user control
                //          homeTeamName: params['homeTeamName'], //don't care to test
                //          startTS: params['startTS'], //checked
                //          description: params['description'],//checked
                //          sendNotification: params['sendNotification'],//doesn't appear in response
                //          recipients: params['recipients'],//doesn't appear in response
                //          durationMinutes: params['durationMinutes'],//check via End key
                //          awayTeamName: params['awayTeamName'], //checked
                //          resourceId: params['resourceId'], //checked via resourceIdSSU
                //          type: params['type'], //checked
                //          awayTeamGuid: params['awayTeamGuid'],//blank param, no user control
                //          teamGuid: params['teamGuid']//checked with homeTeamId

                //     },{json: true})
                //     //.inspectJSON()
                //     .expectJSON('*',{
                //         //Name:params['eventName'],
                //         Description : params['description'],
                //         Start:params['startTS_String'],
                //         End:params['endTS_String'],
                //         Type:params['type'],
                //         HomeTeam:{
                //             Id:params['homeTeamId']
                //         },
                //         AwayTeam:{
                //             Name:params['awayTeamName']
                //         },
                //         Resource:{
                //             Id:params['resourceIdSSU']
                //         }
                //     })
                // .toss();

               }) 
            .toss();
*/

    })
    .toss();
