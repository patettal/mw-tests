
var frisby = require('frisby');
// Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});

//there should be better ways to do this!
//FOUND IT!       _frisbyGlobalSetup.request.baseUri = ''; GOTTA CHANGE EVERYTHING NOW...
var URL = 'https://middleware-player.siplay.com'; //prod
//var URL = 'https://middleware-player.siplay.xyz'; //xyz
var responsetime=10000; 


//Test matrix and notes found at https://docs.google.com/spreadsheets/d/1qQBHur6CGYGsGvcirif-oLTXF56Udv-L2IafOd4tqZk/edit#gid=1381422522

//SSU TESTS--------------------------------------------------------------------------------------------------------------------------------------------------------

/*
EXPLANATIONS FROM BRETT IN JIRA ISSUE
                name: 'Session Finished SSU 1',
                status: 1,  // All along there has been a "status" field being returned for each team. That field was always a 1 because the 
                            //teams returned were always active. However, it can now be additional values:
                            //1 = active
                            //2 = inactive (under construction)
                            //3 = coach preview
                team_active: true,  //will be either true or false. This alone can be used right now to determine whether the team details 
                                    //should be displayed in the UI. If team_active is false, the team name should only be shown in the UI 
                                    //for past season teams. For current or future season teams, the team name should NOT be shown. You can 
                                    //show the "league" value and the "session start / end dates" as well and the person that is on the team 
                                    //will be available in the roster.players array or the roster.volunteers array. You can compare those 
                                    //player/volunteer guids to the account members (the selectable players for an account) to determine which 
                                    //people in the user's account are members of the inactive teams.

                status_name: 'active', //This is informational and will hold the word "active" for any active teams. 
                                       //For an inactive team, it might be "inactive" or "coachpreview", but could 
                                       //potentially be a different string value <> "active" as well (future statuses or LA values)
                session_active: false   //a team can be inactive even if its session is active
*/

//Active Team
var email = 'ssu1%40ses.siplay.io';
var password = 'SIPtest1';


// frisby.globalSetup({
//     request: {
//         headers: {},
//         inspectOnFailure: true,
//         baseUri: ''
//     }
// })

frisby.create("User in active team - Login")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("User in active team - Teams check")
            .get(URL + "/teams")
            //.inspectJSON()
            .expectJSON('?',{ //OLD SESSION
                ss: 'SS',
                ssid: 727288,
                sipid: 'SS-727288',
                manager_nm: 'SIP Tests league',
                
                name: 'Session Finished SSU 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Session Old 2016 test',
                season_starts: 1470009600000, //08/01/2016
                season_ends: 1474070340000, //09/16/2016
             })

            .expectJSON('?',{ //CURRENT SESSION
                ss: 'SS',
                ssid: 727284,
                sipid: 'SS-727284',
                manager_nm: 'SIP Tests league',
                
                name: 'SSU Team 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: true,
                
                league: 'SIP Tests league',
                season: 'Active Session 2016',
                season_starts: 1472688000000, //09/01/2016
                season_ends: 1493942340000 //05/04/2017
            })

            .expectJSON('?',{ //FUTURE SESSION
                ss: 'SS',
                ssid: 727286,
                sipid: 'SS-727286',
                manager_nm: 'SIP Tests league',

                name: 'Session Not Started SSU 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Future Session 2017',
                season_starts: 1483228800000, //01/01/2017 
                season_ends: 1488412740000 //03/01/2017
            })
            //.inspectJSON()
            .toss();

        frisby.create("User in active team - Teams?include... check")
            .get(URL + "/teams?includeInactiveSessions=true")
            //.inspectJSON()
            
            .expectJSON('?',{ //OLD SESSION
                ss: 'SS',
                ssid: 727288,
                sipid: 'SS-727288',
                manager_nm: 'SIP Tests league',
                
                name: 'Session Finished SSU 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Session Old 2016 test',
                season_starts: 1470009600000,
                season_ends: 1474070340000,
            })
            
            .expectJSON('?',{ //CURRENT SESSION
                ss: 'SS',
                ssid: 727284,
                sipid: 'SS-727284',
                manager_nm: 'SIP Tests league',
                
                name: 'SSU Team 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: true,
                
                league: 'SIP Tests league',
                season: 'Active Session 2016',
                season_starts: 1472688000000,
                season_ends: 1493942340000
            })

            .expectJSON('?',{ //FUTURE SESSION
                ss: 'SS',
                ssid: 727286,
                sipid: 'SS-727286',
                manager_nm: 'SIP Tests league',

                name: 'Session Not Started SSU 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Future Session 2017',
                season_starts: 1483228800000,
                season_ends: 1488412740000
            })
            .toss();
    })
    .toss();

//Inactive Team
var email = 'ssu2%40ses.siplay.io';
var password = 'SIPtest1';

frisby.create("User in inactive team - Login")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("User in inactive team - Teams check")
            .get(URL + "/teams")
            .expectBodyContains('[]')
            .toss();

        frisby.create("User in inactive team - Teams?include... check")
            .get(URL + "/teams?includeInactiveSessions=true")
            //.inspectJSON()
            
            .expectJSON('?',{ //OLD SESSION
                ss: 'SS',
                ssid: 727289,
                sipid: 'SS-727289',
                manager_nm: 'SIP Tests league',
                
                name: 'Session Finished SSU 2',
                status: 2,
                team_active: false,
                status_name: 'inactive',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Session Old 2016 test',
                season_starts: 1470009600000,
                season_ends: 1474070340000,
            })
            //.inspectJSON()
            .expectJSON('?',{ //CURRENT SESSION
                ss: 'SS',
                ssid: 727285,
                sipid: 'SS-727285',
                manager_nm: 'SIP Tests league',
                
                name: 'SSU Team 2',
                status: 2,
                team_active: false,
                status_name: 'inactive',
                session_active: true,
                
                league: 'SIP Tests league',
                season: 'Active Session 2016',
                season_starts: 1472688000000,
                season_ends: 1493942340000
            })

            .expectJSON('?',{ //FUTURE SESSION
                ss: 'SS',
                ssid: 727287,
                sipid: 'SS-727287',
                manager_nm: 'SIP Tests league',

                name: 'Session Not Started SSU 2',
                status: 2,
                team_active: false,
                status_name: 'inactive',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Future Session 2017',
                season_starts: 1483228800000,
                season_ends: 1488412740000
            })
            .toss();
    })
    .toss();

//Coach Preview Team
var email = 'ssu3%40ses.siplay.io';
var password = 'SIPtest1';

frisby.create("User in Coach Preview team - Login")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("User in Coach Preview team - Teams check")
            .get(URL + "/teams")
            //.inspectRequest()
            .expectBodyContains('[]')
            .toss();

        frisby.create("User in Coach Preview team - Teams?include... check")
            .get(URL + "/teams?includeInactiveSessions=true")
            //.inspectJSON()
            
            .expectJSON('?',{ //OLD SESSION
                ss: 'SS',
                ssid: 731786,
                sipid: 'SS-731786',
                manager_nm: 'SIP Tests league',
                
                name: 'Session Finished SSU 3',
                status: 3,
                team_active: false,
                status_name: 'coachpreview',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Session Old 2016 test',
                season_starts: 1470009600000,
                season_ends: 1474070340000
            })
            
            .expectJSON('?',{ //CURRENT SESSION
                ss: 'SS',
                ssid: 731784,
                sipid: 'SS-731784',
                manager_nm: 'SIP Tests league',
                
                name: 'SSU Team 3',
                status: 3,
                team_active: false,
                status_name: 'coachpreview',
                session_active: true,
                
                league: 'SIP Tests league',
                season: 'Active Session 2016',
                season_starts: 1472688000000,
                season_ends: 1493942340000
            })

            .expectJSON('?',{ //FUTURE SESSION
                ss: 'SS',
                ssid: 731785,
                sipid: 'SS-731785',
                manager_nm: 'SIP Tests league',

                name: 'Session Not Started SSU 3',
                status: 3,
                team_active: false,
                status_name: 'coachpreview',
                session_active: false,

                league: 'SIP Tests league',
                season: 'Future Session 2017',
                season_starts: 1483228800000,
                season_ends: 1488412740000
            })
            .toss();
    })
    .toss();

//LA TESTS--------------------------------------------------------------------------------------------------------------------------------------------------------


//Active Team in old session
//https://leagueathletics.com/Roster.asp?myteam=409359&org=siptester5
var email = 'la10%40ses.siplay.io';
var email = 'la5%40ses.siplay.io';
var email = 'la6%40ses.siplay.io';

var password = 'SIPtest1';

frisby.create("User in active team in old session - Login")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("User in active team in old session - Teams check")
            .get(URL + "/teams")
            
            // .expectJSON('?',{ //OLD SESSION
            //     ss: 'LA',
            //     ssid: 409359,
            //     sipid: 'LA-409359-23285',
            //     //manager_nm: 'Tester Three',
                
            //     name: 'Session Finished LA 1',
            //     status: 1,
            //     team_active: true,
            //     status_name: 'active',
            //     session_active: false,
                
            //     league: 'SIP tests',
            //     //season: 'Season Finished',
            //     //season_starts: 1471910400000,
            //     //season_ends: 1505865600000
            // })

            .toss();

        frisby.create("User in active team in old session - Teams?include... check")
            .get(URL + "/teams?includeInactiveSessions=true")            
            // .expectJSON('?',{ //OLD SESSION
            //     ss: 'LA',
            //     ssid: 409359,
            //     sipid: 'LA-409359-23285',
            //     //manager_nm: 'Tester Three',
                
            //     name: 'Session Finished LA 1',
            //     status: 1,
            //     team_active: true,
            //     status_name: 'active',
            //     session_active: false,
                
            //     league: 'SIP tests',
            //     //season: 'Season Finished',
            //     //season_starts: ,
            //     //season_ends: 
            // })
            .toss();
    })
    .toss();
    

//Active Team in current session
var email = 'la2%40ses.siplay.io';
var password = 'SIPtest1';

frisby.create("User in active team in current session - Login")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })
        
        frisby.create("User in active team in current session - Teams check")
            .get(URL + "/teams")
            //.inspectJSON()
            .expectJSON('?',{ //CURRENT SESSION
                ss: 'LA',
                ssid: 402884,
                sipid: 'LA-402884-23285',
                manager_nm: 'Tester Three',
                
                name: 'LA Team Name 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: true,
                
                league: 'SIP tests',
                //season: 'Test Active Season',
                season_starts: 1471910400000, //08/23/2016
                season_ends: 1505865600000 //09/20/2017
            })

            .toss();

        frisby.create("User in active team in current session - Teams?include... check")
            .get(URL + "/teams?includeInactiveSessions=true")
            //.inspectJSON()
            
            .expectJSON('?',{ //CURRENT SESSION
                ss: 'LA',
                ssid: 402884,
                sipid: 'LA-402884-23285',
                manager_nm: 'Tester Three',
                
                name: 'LA Team Name 1',
                status: 1,
                team_active: true,
                status_name: 'active',
                session_active: true,
                
                league: 'SIP tests',
                //season: 'Test Active Season',
                season_starts: 1471910400000, //08/23/2016
                season_ends: 1505865600000 //09/20/2017
            })
            .toss();
        
    })
    .toss();

//Active Team in future session
var email = 'la7%40ses.siplay.io';
var password = 'SIPtest1';

frisby.create("User in active team in future session - Login")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("User in active team in future session - Teams check")
            .get(URL + "/teams")
            //.inspectJSON()
            .expectJSON('?',{ //FUTURE SESSION
                ss: 'LA',
                ssid: 409404,
                sipid: 'LA-409404-23285',
                manager_nm: 'Tester Ten',
                
                name: 'Session Not Started LA 1',
                //status: 1,
                team_active: true,
                status_name: 'active',
                // session_active: false,
                
                league: 'SIP tests',
                //season: 'Test Active Season',
                season_starts: 1504224000000, //09/01/2017
                season_ends: 1505952000000, //09/21/2017
            })

            .toss();

        frisby.create("User in active team in future session - Teams?include... check")
            .get(URL + "/teams?includeInactiveSessions=true")
            .expectJSON('?',{ //FUTURE SESSION
                ss: 'LA',
                ssid: 409404,
                sipid: 'LA-409404-23285',
                manager_nm: 'Tester Ten',
                
                name: 'Session Not Started LA 1',
                //status: 1,
                team_active: true,
                status_name: 'active',
                // session_active: false,
                
                league: 'SIP tests',
                //season: 'Test Active Season',
                season_starts: 1504224000000, //09/01/2017
                season_ends: 1505952000000, //09/21/2017
            })

            .toss();
    })
    .toss();