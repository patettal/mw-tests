/**
 * frisby.js: Facebook usage example
 * (C) 2012, Vance Lucas
 */
//var frisby = require('../lib/frisby');
//var frisby = require('node_modules/frisby/lib/frisby');
var frisby = require('frisby');

// Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});

//there should be better ways to do this!
var URL = 'https://middleware-player.siplay.xyz';
var responsetime=10000; //milliseconds requirement keeps increasing for testing reasons, need to revisit...

//success case
var email = 'luciaines.patetta%40gmail.com';
var password = 'SIPtest2';
//var token = 'c5a2299c4751349f227bae58f83f7f5bf90d48e810bdc60361d197410280ae4e887869208ce8db6e438506ae421973715692baaa866888cba975759e552c2a91';
var currentVersion = '1.07.001'; // TODO: I'd like to really plug this in...
var teamid = 'SS-450221'; 
var teamGuid = teamid;
var teamGuids = [teamid];
//var playerGuid= 8679713;
var teamsWithRostersLimit = 2;
var playerGuid= 'SS-8679713';
var playerGuids=[playerGuid];
var gameGuid='LA-123'; //TBD...
var eventGuid='LA-123'; //TBD...

frisby.create('Login to '+email)
  .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
  .expectStatus(200)
  .expectHeaderContains('content-type', 'application/json')
  .expectJSONTypes({
    dt: String,
    ts: Number,
    success: Boolean,
    message: {
      token: String,
      sipid: String
    }
  })
  .expectJSON({
    success: true
  })
  .expectMaxResponseTime(responsetime)
  //.inspectJSON() //console output displays JSON response, useful for debugging!
  .after(function (err, res, body) {
    var obj = JSON.parse(body);  
    var token = obj['message']['token'];
    frisby.globalSetup({
      timeout: responsetime,
      request: { 
        headers: { 'authentication-token': token }
      }
    });

     frisby.create("Needs Update question")
        .get(URL + "/needsupdate/" + currentVersion)
        .expectStatus(200)
        .toss();

    frisby.create("Set Favorite Teams")
        .get(URL + "/favorites/addteam", {team:teamGuids,forceCache:true})
        .expectStatus(200)
        .toss();

    frisby.create("Remove Favorite Teams")
        .get(URL + "/favorites/removeteam", {team:teamGuids})
        .expectStatus(200)
        .toss();

    frisby.create("Get Favorite teams")
        .get(URL + "/favorites/userteams?includeCounts=1")
        .expectStatus(200)
        .toss();

    frisby.create("Remove all favorite teams")
        .get(URL + "/favorites/removeallteams")
        .expectStatus(200)
        .toss();


/* Commenting because I don't know how the Params are handled. 
    frisby.create("Search Teams")
        .get(URL + "/teams/search")
        .expectStatus(200)
        .toss();
*/

/* Commenting because I don't know how the Params are handled. 
    frisby.create("Search Players")
        .get(URL + "/players/search")
        .expectStatus(200)
        .toss();
*/

    frisby.create("Get Teams With Rosters")
        .get(URL + "/teams/rosters?limit=" + teamsWithRostersLimit)
        .expectStatus(200)
        .toss();

    frisby.create("Get Team info")
        .get(URL + "/teams/" + teamGuid)
        .expectStatus(200)
        .toss();

    frisby.create("Get all teams, include inactive")
        .get(URL + "/teams?includeInactiveSessions=true")
        .expectStatus(200)
        .toss();

    frisby.create("Get all teams")
        .get(URL + "/teams")
        .expectStatus(200)
        .toss();

    frisby.create("Get Team Standings")
        .get(URL + "/team/"+teamGuid+"/standings")
        .expectStatus(200)
        .toss();

    frisby.create("Get Team Albums")
        .get(URL + "/team/"+teamGuid+"/albums?includeVideo=1&includePhotos=1")
        .expectStatus(200)
        .toss();

    frisby.create("Get event albums")
        .get(URL + "/event/"+eventGuid+"/albums?includeVideo=1&includePhotos=1")
        .expectStatus(200)
        .toss();

    frisby.create("Get player album")
        .get(URL + "/player/"+playerGuid+"/albums?includeVideo=1&includePhotos=1")
        .expectStatus(200)
        .toss();

    frisby.create("Set Favorite Players")
        .get(URL + "/favorites/addplayer", {player:playerGuids,forceCache:true})
        .expectStatus(200)
        .toss();

    frisby.create("Remove Favorite Players")
        .get(URL + "/favorites/removeplayer", {player:playerGuids})
        .expectStatus(200)
        .toss();

    frisby.create("Get Favorite Players")
        //options parameter still an unknown...
        .get(URL + "/favorites/userplayers")
        .expectStatus(200)
        .toss();

    frisby.create("Remove all Favorite Players")
        .get(URL + "/favorites/removeallplayers")
        .expectStatus(200)
        .toss();


    frisby.create("Get Players")
        .get(URL + "/players")
        .expectStatus(200)
        .toss();

    frisby.create("Get Player History")
        .get(URL + "/player/" + playerGuid)
        .expectStatus(200)
        .toss();

    frisby.create("Get Game Status")
        //Retrieve the status of games that are scored with iScore
        .get(URL + "/game/status/" + gameGuid)
        .expectStatus(200)
        .toss();

    frisby.create("Invite players to team")
        .get(URL + "/teams/roster/invite", {team_id:teamGuid,user_ids:playerGuids})
        .expectStatus(200)
        .toss();

    frisby.create("Decline invite")
        //Tells the server that the user is not interested in inviting team members to use the SIP application.
        .get(URL + "/teams/"+teamGuid+"/decline-invite")
        .expectStatus(200)
        .toss();

/* Commenting because I don't know how the Params are handled. 
    frisby.create("Set Game Status")
        .post(URL + "/game/status/" + gameGuid)
        .expectStatus(200)
        .toss();
*/

    frisby.create("Get Unread chat message count")
        .get(URL + "/unreadmessages/")
        .expectStatus(200)
        .toss();

    frisby.create("Get events")
        .get(URL + "/events", {forceRefresh:false})
        .expectStatus(200)
        .toss();

    frisby.create("Get events for a team")
        .get(URL + "/events/"+teamGuid)
        .expectStatus(200)
        .toss();

    frisby.create("Get event by ID")
        .get(URL + "/event/" + eventGuid + "/" + teamGuid)
        .expectStatus(200)
        .toss();

    frisby.create("Get User Tasks")
        .get(URL + "/tasks")
        .expectStatus(200)
        .toss();

    frisby.create("Get User Tasks for Event")
        .get(URL + "/tasks/event/"+eventGuid)
        .expectStatus(200)
        .toss();

    frisby.create("Get user's main feed")
        //the /userfeed "/userfeed" now has optional start= and count= parameters to retrieve paged results.
        .get(URL + "/userfeed")
        .expectStatus(200)
        .toss();

    frisby.create("Get user's created feed")
        //the /userfeed "/userfeed/created" now has optional start= and count= parameters to retrieve paged results.
        .get(URL + "/userfeed/created")
        .expectStatus(200)
        .toss();



})
.toss();//login
