
var frisby = require('frisby');
// Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});

//there should be better ways to do this!
//var URL = 'https://middleware-player.siplay.com'; //prod
var URL = 'https://middleware-player.siplay.xyz'; //xyz
var responsetime=10000; 


//existing user with XREF in LA and SS
var email = 'brett.law%40timeinc.com';
var password = 'letmein0';

frisby.create("Login to existing user with XREF in LA and SS")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("XREF user has LA teams and SS teams")
            .get(URL + "/teams")
            //.inspectJSON()
            .expectJSON('?',{
                ss:"LA"
            })
            .expectJSON('?',{
                ss:"SS"
            })
            .toss();
    })
    .toss();

//existing user with XREF in LA
var email = 'la10%40ses.siplay.io';
var password = 'SIPtest1';

frisby.create("Login to existing user with XREF in LA")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("XREF user has only LA teams")
            .get(URL + "/teams")
            .expectJSON('*',{
                ss:"LA"
            })
            .toss();
    })
    .toss();

//existing user with XREF in SS
var email = 'ssu8%40ses.siplay.io';
var password = 'SIPtest1';

frisby.create("Login to existing user with XREF in SS")
    .get(URL+'/authentication/customers/login?email='+email+'&password='+password)
    .expectStatus(200)
    .after(function (err, res, body) {
        var obj = JSON.parse(body);  
        var token = obj['message']['token'];
        frisby.globalSetup({
          timeout: responsetime,
          request: { 
            headers: { 'authentication-token': token }
          }
        })

        frisby.create("XREF user has only SS teams")
            .get(URL + "/teams")
            .expectJSON('*',{
                ss:"SS"
            })
            .toss();
    })
    .toss();

//Pending data generation scripts:
/*
5. new user only exists in LA          
6. new user only exists in SS          
7. new user exists in both LA and SS with same password 
8. new user exists in both LA and SS with different passwords 
9. new user exists in both LA and SS with different passwords
*/